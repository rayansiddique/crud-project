<?php

namespace App\Http\Controllers;

use App\Http\Requests\BrandRequest;
use App\Models\Brand;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class BrandController extends Controller
{
    public function index()
    {
        $brands = Brand::latest()->get();
        // dd($categories);
        return view('backend.brands.index', [
            'brands' => $brands
        ]);
    }

    public function create()
    {
        return view('backend.brands.create');
    }

    public function store(Request $request)
    {

        try {
            // $request->validate([
            //     'title' => 'required|min:3|max:50|unique:categories,title',
            //     // 'title' => ['required', 'min:3', Rule::unique('categories', 'title')],
            //     'description' => ['required', 'min:10'],
            // ]);
            // $imageName = request()->file('image')->store('storage/app/public/images');
            // dd($imageName);
            Brand::create([
                'title' => $request->title,
                'description' => $request->description,
            ]);

            // $request->session()->flash('message', 'Task was successful!');
            return redirect()->route('brands.index')->withMessage('Successfully Created!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
            // dd($e->getMessage());
        }
    }

    // public function show(Category $id)
    public function show(Brand $brand)
    {
        // $category = Category::where('id', $id)->firstOrFail();
        // $category = Category::find($id);
        return view('backend.brands.show', [
            'brand' => $brand
        ]);
    }

    public function edit(Brand $brand)
    {
        return view('backend.brands.edit', [
            'brand' => $brand
        ]);
    }

    public function update(BrandRequest $request, Brand $brand)
    {
        try {
            // $request->validate([
            //     // 'title' => 'required|min:3|max:50|unique:categories,title,'.$category->id,
            //     'title' => ['required', 'min:3', 
            //         Rule::unique('categories', 'title')->ignore($category->id)
            //     ],
            //     'description' => ['required', 'min:10'],
            // ]);

            $brand->update([
                'title' => $request->title,
                'description' => $request->description,
            ]);

            // $request->session()->flash('message', 'Task was successful!');
            return redirect()->route('brands.index')->withMessage('Successfully Updated!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
            // dd($e->getMessage());
        }
    }

    public function destroy(Brand $brand)
    {
        try {
            $brand->delete();
            return redirect()->route('brands.index')->withMessage('Successfully Deleted!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
            // dd($e->getMessage());
        }
    }
}
